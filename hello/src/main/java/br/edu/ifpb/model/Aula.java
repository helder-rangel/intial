package br.edu.ifpb.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

@Entity
public class Aula {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotBlank
	@Size(max = 100)
	private String assunto;
	@NotNull
	
	protected Date data;
	
	public Aula(Long id, String assunto, Date data) {
		super();
		this.id = id;
		this.assunto = assunto;
		this.data = data;
	}

	public Aula() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Aula [id=" + id + ", assunto=" + assunto + ", data=" + data + "]";
	}
	
	
	

}
