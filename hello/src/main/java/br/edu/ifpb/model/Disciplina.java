package br.edu.ifpb.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.Entity;
import org.springframework.data.annotation.Id;

@Entity
public class Disciplina {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private Turma codigo;
	
	public Disciplina(Long id, String nome, Turma codigo) {
		super();
		this.id = id;
		this.nome = nome;
		this.codigo = codigo;
	}

	protected Disciplina() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Turma getCodigo() {
		return codigo;
	}

	public void setCodigo(Turma codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "Disciplina [id=" + id + ", nome=" + nome + ", codigo=" + codigo + "]";
	}
	
	

}
