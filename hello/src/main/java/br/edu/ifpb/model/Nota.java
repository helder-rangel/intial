package br.edu.ifpb.model;

import java.util.Arrays;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.Entity;
import org.springframework.data.annotation.Id;

@Entity
public class Nota {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private int[] notas;
	private int numNotas;
	
	public Nota(Long id, int[] notas, int numNotas) {
		super();
		this.id = id;
		this.notas = notas;
		this.numNotas = numNotas;
	}

	protected Nota() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int[] getNotas() {
		return notas;
	}

	public void setNotas(int[] notas) {
		this.notas = notas;
	}

	public int getNumNotas() {
		return numNotas;
	}

	public void setNumNotas(int numNotas) {
		this.numNotas = numNotas;
	}

	@Override
	public String toString() {
		return "Nota [id=" + id + ", notas=" + Arrays.toString(notas) + ", numNotas=" + numNotas + "]";
	}

	
}
