package br.edu.ifpb.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.hibernate.annotations.Entity;
import org.springframework.data.annotation.Id;

@Entity
public class Coordenador {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private Usuario login;
	private Usuario senha;
	
	public Coordenador(Long id, String nome, Usuario login, Usuario senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.login = login;
		this.senha = senha;
	}
	
	protected Coordenador() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Usuario getLogin() {
		return login;
	}

	public void setLogin(Usuario login) {
		this.login = login;
	}

	public Usuario getSenha() {
		return senha;
	}

	public void setSenha(Usuario senha) {
		this.senha = senha;
	}

	@Override
	public String toString() {
		return "Coordenador [id=" + id + ", nome=" + nome + ", login=" + login + ", senha=" + senha + "]";
	}
	

}
