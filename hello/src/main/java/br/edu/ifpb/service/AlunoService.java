package br.edu.ifpb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifpb.model.Aluno;
import br.edu.ifpb.repository.IAlunoRepository;

@Service
public class AlunoService {
	
	@Autowired
	private IAlunoRepository alunoRepository;
	
	@Autowired
	public AlunoService(IAlunoRepository alunoRepository) {
		this.alunoRepository = alunoRepository;
	}
	
}
